package com.online.library.config;

import com.online.library.dao.PersonDAOImpl;
import com.online.library.service.PersonService;
import com.online.library.service.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.springframework.http.HttpMethod.*;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new PersonServiceImpl();
    }



    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    ;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http

                .httpBasic().and()
                .authorizeRequests()
                .antMatchers(GET, "/api/persons").hasAuthority("ROLE_librarian")
                .antMatchers(GET, "/api/persons/*").hasAuthority("ROLE_librarian")
                .antMatchers(GET, "/api/books").hasAnyAuthority("ROLE_librarian", "ROLE_USER")
                .antMatchers(GET, "/api/books/*").hasAnyAuthority("ROLE_librarian", "ROLE_USER")
                .antMatchers(GET, "/api/borrows").hasAuthority("ROLE_librarian")
                .antMatchers(GET, "/api/borrows/*").hasAuthority("ROLE_librarian")

                .antMatchers(POST, "/api/books", "/api/books/").hasAuthority("ROLE_librarian")
                .antMatchers(POST, "/api/persons", "/api/persons/").permitAll()
                .antMatchers(POST, "/api/borrows").hasAnyAuthority("ROLE_librarian", "ROLE_USER")

                .antMatchers(PUT, "/api/books", "/api/books/").hasAuthority("ROLE_librarian")
                .antMatchers(PUT, "/api/persons", "/api/persons/").hasAuthority("ROLE_librarian")
                .antMatchers(PUT, "/api/borrows", "/api/borrows/").hasAuthority("ROLE_librarian")

                .antMatchers(DELETE, "/api/books/*").hasAuthority("ROLE_librarian")
                .antMatchers(DELETE, "/api/persons/*").hasAuthority("ROLE_librarian")
                .antMatchers(DELETE, "/api/borrows/*").hasAuthority("ROLE_librarian")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout().permitAll().logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }
}