package com.online.library.rest;

import com.online.library.dto.PersonDto;
import com.online.library.entity.Person;
import com.online.library.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PersonRestController {

    @Autowired
    private PersonService personService;

    @GetMapping("/persons")
    public List<PersonDto> listAll() {
        return personService.listAll();
    }

    @GetMapping("/persons/{id}")
    public PersonDto findById(@PathVariable int id) {
        PersonDto personDto = personService.findById(id);
        if (personDto == null) {
            throw new RuntimeException("Person with id " + id + "not FOUND");
        }
        return personService.findById(id);
    }

    @PostMapping("/persons")
    public void addPerson(@RequestBody Person person) {
        person.setId(0);
        personService.savePerson(person);
    }

    @PutMapping("/persons")
    public void updatePerson(@RequestBody Person person) {
        personService.savePerson(person);
    }

    @DeleteMapping("/persons/{id}")
    public void deletePerson(@PathVariable int id) {
        personService.deleteById(id);

    }


}
