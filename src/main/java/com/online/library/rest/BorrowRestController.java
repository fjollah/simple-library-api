package com.online.library.rest;

import com.online.library.dao.BorrowDAOImpl;
import com.online.library.dto.BookDto;
import com.online.library.dto.BorrowDto;
import com.online.library.entity.Book;
import com.online.library.entity.BorrowBookRequest;
import com.online.library.entity.BorrowedBooks;
import com.online.library.entity.Book;
import com.online.library.entity.BorrowBookRequest;
import com.online.library.entity.BorrowedBooks;
import com.online.library.entity.Person;
import com.online.library.service.BookService;
import com.online.library.service.BorrowService;
import com.online.library.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BorrowRestController {

    @Autowired
    private BorrowService borrowService;
    @Autowired
    private PersonService personService;
    @Autowired
    private BookService bookService;

    @GetMapping("/borrows")
    public List<BorrowDto> listAll() {
        return borrowService.listAll();
    }

    @GetMapping("/borrows/{id}")
    public BorrowDto findById(@PathVariable int id) {
        return borrowService.findById(id);
    }


    @PostMapping("/borrows")
    public void saveBorrowedBook(@RequestBody BorrowBookRequest request) {
        borrowService.saveBorrowedBook(request);
    }


    @PutMapping("/borrows")
    public void updateBorrowedBook(@RequestBody BorrowBookRequest book) {
        borrowService.saveBorrowedBook(book);
    }

    @DeleteMapping("/borrows/{id}")
    public void deletePerson(@PathVariable int id) {
        borrowService.deleteById(id);
    }
}
