package com.online.library.rest;

import com.online.library.dto.BookDto;
import com.online.library.entity.Book;
import com.online.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BookRestController {

    @Autowired
    private BookService bookService;


    @GetMapping("/books")
    public List<BookDto> listAll() {
        return bookService.listAll();
    }

    @GetMapping("/books/{id}")
    public BookDto findById(@PathVariable int id) {
        BookDto bookDto = bookService.findById(id);
        if (bookDto == null) {
            throw new RuntimeException("book with id " + id + "not FOUND");
        }
        return bookService.findById(id);
    }

    @PostMapping("/books")
    public void addBook(@Valid @RequestBody Book book) {
        book.setId(0);
        bookService.saveBook(book);
    }


    @PutMapping("/books")
    public void updateBook(@RequestBody Book book) {
        bookService.saveBook(book);
    }


    @DeleteMapping("/books/{id}")
    public void deleteBook(@PathVariable int id) {
        bookService.deleteById(id);

    }


}
