package com.online.library.dto;


import com.online.library.entity.Book;
import com.online.library.entity.Person;

import javax.persistence.*;

public class BorrowDto {
    private PersonDto personDto;
    private BookDto bookDto;

    public BorrowDto(PersonDto personDto, BookDto bookDto) {
        this.personDto = personDto;
        this.bookDto = bookDto;
    }

    public BorrowDto() {
    }

    public PersonDto getPersonDto() {
        return personDto;
    }

    public void setPersonDto(PersonDto personDto) {
        this.personDto = personDto;
    }

    public BookDto getBookDto() {
        return bookDto;
    }

    public void setBookDto(BookDto bookDto) {
        this.bookDto = bookDto;
    }
}
