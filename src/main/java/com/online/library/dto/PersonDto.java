package com.online.library.dto;


public class PersonDto {
    private String name;
    private String address;
    private String phone;
    private String userName;

    public PersonDto(String name, String address, String phone, String userName) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.userName = userName;
    }

    public PersonDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
