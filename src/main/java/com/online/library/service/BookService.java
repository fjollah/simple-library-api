package com.online.library.service;

import com.online.library.dto.BookDto;
import com.online.library.entity.Book;

import java.util.List;

public interface BookService {

    public List<BookDto> listAll();

    public BookDto findById(int id);

    public Book saveBook(Book book);

    public void deleteById(int id);
}
