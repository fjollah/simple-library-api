package com.online.library.service;

import com.online.library.dto.BookDto;
import com.online.library.dto.BorrowDto;
import com.online.library.entity.Book;
import com.online.library.entity.BorrowBookRequest;
import com.online.library.entity.BorrowedBooks;

import java.util.List;

public interface BorrowService {
    public List<BorrowDto> listAll();

    public BorrowDto findById(int id);

    public BorrowedBooks saveBorrowedBook(BorrowBookRequest request);

    public void deleteById(int id);
}
