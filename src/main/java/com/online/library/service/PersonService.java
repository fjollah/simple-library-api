package com.online.library.service;

import com.online.library.dto.PersonDto;
import com.online.library.entity.Person;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface PersonService {
    public List<PersonDto> listAll();

    public PersonDto findById(int id);

    public Person savePerson(Person person);

    public void deleteById(int id);

    UserDetails loadUserByUsername(String username);

}
