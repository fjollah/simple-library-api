package com.online.library.service;

import com.online.library.dao.BookDAO;
import com.online.library.dto.BookDto;
import com.online.library.entity.Book;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDAO bookDAO;

    @Override
    @Transactional
    public List<BookDto> listAll() {
        List<Book> books = bookDAO.listAll();
        List<BookDto> booksDTO = new ArrayList<>();

        for (Book book : books) {

            ModelMapper bookMapper = new ModelMapper();
            BookDto bookDto = new BookDto();
            bookMapper.map(book, bookDto);
            booksDTO.add(bookDto);
        }

        return booksDTO;

    }

    @Override
    @Transactional
    public BookDto findById(int id) {
        Book book = bookDAO.findById(id);
        ModelMapper bookMapper = new ModelMapper();
        BookDto bookDto = new BookDto();
        bookMapper.map(book, bookDto);
        return bookDto;
    }

    @Override
    @Transactional
    public Book saveBook(Book book) {

        Book update_book = bookDAO.findById(book.getId());
        if (update_book == null) {
            return bookDAO.saveBook(book);
        } else {
            book.setId(update_book.getId());
            book.setAuthor(book.getAuthor() == null ? update_book.getAuthor() : book.getAuthor());
            book.setTitle(book.getTitle() == null ? update_book.getTitle() : book.getTitle());
            book.setIsbn(book.getIsbn() == null ? update_book.getIsbn() : book.getIsbn());
            book.setQuantity(book.getQuantity() == 0 ? update_book.getQuantity() : book.getQuantity());
            book.setBorrowedBooks(update_book.getBorrowedBooks());
            return bookDAO.saveBook(book);
        }
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        bookDAO.deleteById(id);

    }
}
