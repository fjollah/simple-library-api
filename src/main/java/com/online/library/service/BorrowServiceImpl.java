package com.online.library.service;

import com.online.library.dao.BookDAO;
import com.online.library.dao.BorrowDAO;
import com.online.library.dao.PersonDAO;
import com.online.library.dto.BookDto;
import com.online.library.dto.BorrowDto;
import com.online.library.dto.PersonDto;
import com.online.library.entity.Book;
import com.online.library.entity.BorrowBookRequest;
import com.online.library.entity.BorrowedBooks;
import com.online.library.entity.Person;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Service
public class BorrowServiceImpl implements BorrowService {

    @Autowired
    private BorrowDAO borrowDAO;

    @Autowired
    private PersonService personService;

    @Autowired
    private BookService bookService;


    @Autowired
    private BookDAO bookDAO;
    @Autowired
    private PersonDAO personDAO;

    @Override
    @Transactional
    public List<BorrowDto> listAll() {

        List<BorrowedBooks> borrowedBooksList = borrowDAO.listAll();
        List<BorrowDto> borrowDtoList = new ArrayList<>();
        for (BorrowedBooks borrowedBook : borrowedBooksList) {

            BookDto bookDto = new BookDto();
            PersonDto personDto = new PersonDto();
            BorrowDto borrowDto = new BorrowDto();
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.map(borrowedBook.getBook(), bookDto);
            modelMapper.map(borrowedBook.getPerson(), personDto);
            borrowDto.setBookDto(bookDto);
            borrowDto.setPersonDto(personDto);
            borrowDtoList.add(borrowDto);
        }
        return borrowDtoList;
    }

    @Override
    @Transactional
    public BorrowDto findById(int id) {
        BorrowedBooks borrowedBook = borrowDAO.findById(id);
        BookDto bookDto = new BookDto();
        PersonDto personDto = new PersonDto();
        BorrowDto borrowDto = new BorrowDto();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(borrowedBook.getBook(), bookDto);
        modelMapper.map(borrowedBook.getPerson(), personDto);
        borrowDto.setBookDto(bookDto);
        borrowDto.setPersonDto(personDto);
        return borrowDto;
    }


    public Boolean checkAvailable(BorrowBookRequest request) {
        Boolean check = true;
        BookDto bookDto = bookService.findById(request.bookId);
        PersonDto personDto = personService.findById(request.personId);
        List<BorrowedBooks> checkborrowedBooks = borrowDAO.listAll();
        List<BorrowedBooks> checkList = new ArrayList<>();
        for (BorrowedBooks checkborrowedBook : checkborrowedBooks) {
            if (checkborrowedBook.getBook().getId() == request.bookId) {
                checkList.add(checkborrowedBook);
            }
        }
        if (checkList.size() >= bookDto.getQuantity()) {
            check = false;


        }
        return check;
    }

    //@RequestBody @Valid
    @Override
    @Transactional
    public BorrowedBooks saveBorrowedBook(BorrowBookRequest request) {
        Boolean check = checkAvailable(request);
        if (check == false) {
            throw new RuntimeException("No books available");
        } else {
            BorrowedBooks requestedBook = new BorrowedBooks();
            Book book = bookDAO.findById(request.bookId);
            Person person = personDAO.findById(request.personId);
            requestedBook.setPerson(person);
            requestedBook.setBook(book);
            return borrowDAO.saveBook(requestedBook);
        }
    }

    //    public void updateBorrowedBook(BorrowBookRequest request) {
//        if (findById(request.bookId) == null) {
//            throw new RuntimeException("book doesnt exits");
//        }
//        saveBorrowedBook(request);
//    }
    @Override
    @Transactional
    public void deleteById(int id) {
        borrowDAO.deleteById(id);

    }
}