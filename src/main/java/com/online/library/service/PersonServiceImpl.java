package com.online.library.service;

import com.online.library.dao.PersonDAO;
import com.online.library.dto.PersonDto;
import com.online.library.entity.Person;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
public class PersonServiceImpl implements PersonService, UserDetailsService {


    @Autowired
    private PersonDAO personDAO;

    @Autowired
    private EntityManager entityManager;

    @Override
    @Transactional
    public List<PersonDto> listAll() {
        List<Person> personList = personDAO.listAll();
        List<PersonDto> personDtoList = new ArrayList<>();

        for (Person person : personList) {

            ModelMapper bookMapper = new ModelMapper();
            PersonDto personDto = new PersonDto();
            bookMapper.map(person, personDto);
            personDtoList.add(personDto);
        }

        return personDtoList;

    }

    @Override
    @Transactional
    public PersonDto findById(int id) {
        Person person = personDAO.findById(id);
        ModelMapper personMapper = new ModelMapper();
        PersonDto personDto = new PersonDto();
        personMapper.map(person, personDto);
        return personDto;
    }

    @Override
    @Transactional
    public Person savePerson(Person person) {
        Person update_person = personDAO.findById(person.getId());
        if (update_person == null) {
            return personDAO.savePerson(person);
        } else {

            person.setId(update_person.getId());
            person.setName(person.getName() == null ? update_person.getName() : person.getName());
            person.setAddress(person.getAddress() == null ? update_person.getAddress() : person.getAddress());
            person.setUserName(person.getUserName() == null ? update_person.getUserName() : person.getUserName());
            person.setRole(person.getRole() == null ? update_person.getRole() : person.getRole());
            person.setPhone(person.getPhone() == null ? update_person.getPhone() : person.getPhone());
            person.setPassword(person.getPassword() == null ? update_person.getPassword() : person.getPassword());
            person.setBorrowedBooks(update_person.getBorrowedBooks());
            return personDAO.savePerson(person);


        }

    }


    @Override
    @Transactional
    public void deleteById(int id) {
        personDAO.deleteById(id);

    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Person optionalUser = personDAO.findByUserName(username);
        User.UserBuilder builder = null;
        if (optionalUser != null) {
            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.password(new BCryptPasswordEncoder().encode(optionalUser.getPassword()));
            builder.roles(optionalUser.getRole());
        } else {
            throw new UsernameNotFoundException("User not found.");
        }

        return builder.build();

    }
}
