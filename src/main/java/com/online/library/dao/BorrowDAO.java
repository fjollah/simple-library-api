package com.online.library.dao;

import com.online.library.entity.Book;
import com.online.library.entity.BorrowedBooks;

import java.util.List;


public interface BorrowDAO {
    public List<BorrowedBooks> listAll();
    public BorrowedBooks findById(int id);
    public BorrowedBooks saveBook(BorrowedBooks borrowedBook);
    public void deleteById(int id);
}
