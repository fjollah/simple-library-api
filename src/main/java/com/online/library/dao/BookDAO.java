package com.online.library.dao;

import com.online.library.dto.BookDto;
import com.online.library.entity.Book;

import java.util.List;


public interface BookDAO {

    public List<Book> listAll();

    public Book findById(int id);

    public Book saveBook(Book book);

    public void deleteById(int id);
}
