package com.online.library.dao;

import com.online.library.entity.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDAO {

    public List<Person> listAll();

    public Person findById(int id);

    public Person savePerson(Person person);

    public void deleteById(int id);

    //    Optional<Person> findByUserName(String userName);
    Person findByUserName(String userName);


}
