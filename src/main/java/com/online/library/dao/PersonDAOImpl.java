package com.online.library.dao;

import com.online.library.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Repository
public class PersonDAOImpl implements PersonDAO {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Person> listAll() {
        Query thequery = entityManager.createQuery("from Person");
        List<Person> personList = thequery.getResultList();
        return personList;

    }

    @Override
    public Person findById(int id) {
        Person person = entityManager.find(Person.class, id);
        return person;
    }

    @Override
    public Person savePerson(Person person) {
        Person new_person = entityManager.merge(person);
        person.setId(new_person.getId());
        return new_person;
    }

    @Override
    public void deleteById(int id) {
        entityManager.remove(entityManager.find(Person.class, id));
    }

    @Override
    public Person findByUserName(String userName) {
        TypedQuery<Person> thequery = (TypedQuery<Person>) entityManager.createQuery("from Person where userName=:userName");
        thequery.setParameter("userName", userName);
        Person person = thequery.getSingleResult();
        if (thequery == null) {
            throw new RuntimeException("Username not found!");
        }
        return person;

    }
}
