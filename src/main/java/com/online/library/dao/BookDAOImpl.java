package com.online.library.dao;

import com.online.library.dto.BookDto;
import com.online.library.entity.Book;
import com.online.library.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BookDAOImpl implements BookDAO {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Book> listAll() {
        Query query = entityManager.createQuery("from Book");
        List<Book> books = query.getResultList();
        return books;
    }

    @Override
    public Book findById(int id) {
        Book book = entityManager.find(Book.class, id);
        return book;
    }

    @Override
    public Book saveBook(Book book) {
        Book thebook = entityManager.merge(book);
        book.setId(thebook.getId());
        return thebook;
    }

    @Override
    public void deleteById(int id) {
        entityManager.remove(entityManager.find(Book.class, id));
    }
}
