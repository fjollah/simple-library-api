package com.online.library.dao;

import com.online.library.entity.BorrowedBooks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class BorrowDAOImpl implements BorrowDAO {

    @Autowired
    private EntityManager entityManager;

    public List<BorrowedBooks> listAll() {
        Query thequery = entityManager.createQuery("from BorrowedBooks");
        List<BorrowedBooks> borrowedBooksList = thequery.getResultList();
        return borrowedBooksList;
    }

    @Override
    public BorrowedBooks findById(int id) {
        BorrowedBooks borrowedBook = entityManager.find(BorrowedBooks.class, id);
        return borrowedBook;
    }

    @Override
    public BorrowedBooks saveBook(BorrowedBooks borrowedBooks) {
        BorrowedBooks new_request = entityManager.merge(borrowedBooks);
        borrowedBooks.setId(new_request.getId());
        return new_request;
    }

    @Override
    public void deleteById(int id) {
        Query thequery = entityManager.createQuery("delete from BorrowedBooks where id=:borrowed_id");
        thequery.setParameter("borrowed_id", id);
        thequery.executeUpdate();

    }


}
