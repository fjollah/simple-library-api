package com.online.library.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class BorrowBookRequest {

    @JsonProperty
    @NotNull
    public Integer personId;

    @JsonProperty
    @NotNull
    public Integer bookId;
}
