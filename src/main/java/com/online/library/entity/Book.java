package com.online.library.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "book")
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "ISBN number is mandatory")
    @Column(name = "isbn")
    private String isbn;

    @NotBlank(message = "Title is mandatory")
    @Size(min = 2,message = "The title must have at least two characters!")
    @Column(name = "title")
    private String title;

    @NotBlank(message = "Author name is mandatory")
    @Size(min = 2,message = "The name of the author must have at least two characters!")
    @Column(name = "author")
    private String author;

    //@NotBlank(message = "Quantity is mandatory")
    //@Pattern(regexp="^(0|[1-9][0-9]*)$",message = "Quantity must be a number!")
    @Column(name = "quantity")
    private int quantity;


    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = BorrowedBooks.FIELD_BOOK, orphanRemoval = true)
    private List<BorrowedBooks> borrowedBooks = new ArrayList<>();

    public Book() {
    }

    public Book(String isbn, String title, String author, int quantity) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<BorrowedBooks> getBorrowedBooks() {
        return borrowedBooks;
    }

    public void setBorrowedBooks(List<BorrowedBooks> borrowedBooks) {
        this.borrowedBooks = borrowedBooks;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}